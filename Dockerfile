FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} commandes-ms.jar
ENTRYPOINT ["java","-jar","/commandes-ms.jar"]
